/* rfw - reboot to UEFI firmware */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <fcntl.h>

struct efi_guid {
    uint32_t a;
    uint16_t b;
    uint16_t c;
    uint8_t d[8];
};

#define EFI_GLOBAL_VARIABLE \
    (struct efi_guid) { 0x8BE4DF61,0x93CA,0x11d2, \
        { 0xAA,0x0D,0x00,0xE0,0x98,0x03,0x2B,0x8C } }

#define EFI_OS_INDICATIONS_BOOT_TO_FW_UI 1

#define EFI_VAR_NV 1
#define EFI_VAR_BS 2
#define EFI_VAR_RT 4

void show_help()
{
    fputs("Usage: rfw [ -h | -u | -r  ]\n", stderr);
    fputs("  -h   Shows this help menu\n", stderr);
    fputs("  -u   Cancel rebooting to firmware\n", stderr);
    fputs("  -r   Reboot to firmware\n", stderr);
    
    exit(1);
}

int main(int argc, char **argv)
{
    if (argc < 2) 
        show_help();

    int reboot_to_fw = 0;

    opterr = 0;
    int opt;
    
    while((opt = getopt(argc, argv, "hur")) != -1) {
        switch (opt) {
            case 'h': 
            case '?':
                show_help();
                break;
            case 'u':
                reboot_to_fw = 0;
                break;
            case 'r':
                reboot_to_fw = 1;
                break;
        }
    }
    
    int checkfd;

    if((checkfd = open("/sys/firmware/efi/efivars", 0)) < 0) {
        fprintf(stderr, "rfw: EFI variables are not supported\n");
        return 1;
    }

    close(checkfd);

    char efivar_file[77];

    struct efi_guid vendor_guid = EFI_GLOBAL_VARIABLE;

    snprintf(efivar_file, 77, "/sys/firmware/efi/efivars/OsIndications-%08"PRIx32
            "-%04"PRIx16"-%04"PRIx16"-%02"PRIx8"%02"PRIx8"-%02"PRIx8"%02"PRIx8
            "%02"PRIx8"%02"PRIx8"%02"PRIx8"%02"PRIx8, vendor_guid.a, vendor_guid.b, 
            vendor_guid.c, vendor_guid.d[0], vendor_guid.d[1], vendor_guid.d[2], 
            vendor_guid.d[3], vendor_guid.d[4], vendor_guid.d[5], vendor_guid.d[6], vendor_guid.d[7]);

    int efivar_fd;

    if ((efivar_fd = open(efivar_file, O_CREAT | O_RDWR)) < 0) {
        perror("rfw: Failed to access EFI variable");
        return 1;
    }

    char buf[12];
    read(efivar_fd, buf, 12);

    int bitmask = buf[4] & buf[5] << 1 & buf[6] << 2 & buf[7] << 3 & 
        buf[8] << 4 & buf[9] << 5 & buf[10] << 6 & buf[11] << 7;

    if (reboot_to_fw == 1) {
        if ((bitmask & EFI_OS_INDICATIONS_BOOT_TO_FW_UI) == EFI_OS_INDICATIONS_BOOT_TO_FW_UI) 
            return 0;
    
        char wrbuf[12] = {0};

        wrbuf[0] = EFI_VAR_NV | EFI_VAR_BS | EFI_VAR_RT;
        wrbuf[4] = EFI_OS_INDICATIONS_BOOT_TO_FW_UI;

        write(efivar_fd, wrbuf, 12);
    } else {
        if (!((~bitmask & EFI_OS_INDICATIONS_BOOT_TO_FW_UI) == EFI_OS_INDICATIONS_BOOT_TO_FW_UI))
            return 0;

        char wrbuf[12] = {0};

        wrbuf[0] = EFI_VAR_NV | EFI_VAR_BS | EFI_VAR_RT;

        write(efivar_fd, wrbuf, 12);
    }

    return 0;
}
