rfw:
	cc rfw.c -o rfw

clean:
	rm -f rfw

install: rfw
	cp rfw /usr/local/bin

uninstall:
	rm -f /usr/local/bin/rfw
